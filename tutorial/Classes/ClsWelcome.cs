﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using tutorial.Classes;

namespace tutorial
{
    public class ClsWelcome
    {
        public static DataTable TampilData()
        {
            try
            {
                DataTable DtTampil = new DataTable();
                SqlConnection Sqlconn = new SqlConnection(ClsModule.Conn.ToString());
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter Da = new SqlDataAdapter();

                Sqlconn.Open();
                cmd.Connection = Sqlconn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Select * from tblBarang order by kodebarang asc";
                Da.SelectCommand = cmd;
                Da.Fill(DtTampil);
                Sqlconn.Close();
                return DtTampil;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static void SimpanBarang(string namaBarang, string satuan, string kategori)
        {
            try
            {
                SqlConnection Sqlconn = new SqlConnection(ClsModule.Conn.ToString());
                SqlCommand cmd = new SqlCommand();

                Sqlconn.Open();
                cmd.Connection = Sqlconn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "insert into tblBarang values('"+namaBarang+"','" + satuan + "','" + kategori + "')";
                cmd.ExecuteNonQuery();
                Sqlconn.Close();

            }
            catch( Exception)
            {
                throw;
            }
        }
    }
}