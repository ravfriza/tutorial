﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="tutorial.Welcome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Halaman Utama</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="bootstrap/css/jquery.dataTables.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <header>
                         <nav class="navbar navbar-expand-lg navbar-light bg-success ">
              <div class="container-fluid">
                  <!-- Brand -->
                <a class="navbar-brand text-white" href="#">Friend Insurance</a>

                  <!-- Toggle -->
                <button class="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>

                  <!-- Menu bar -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav me-auto mb-2 mb-lg-0 ">
                    <li class="nav-item">
                      <a class="nav-link active text-white" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Dropdown
                      </a>
                      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><hr class="dropdown-divider"/></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                      </ul>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                  </ul>
                  <div class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
                    <button class="btn btn-outline-primary text-white" type="submit">Search</button>
                  </div>
                </div>
              </div>
                  <!-- /Menu bar -->

            </nav>
        </header>
        <main role="main">
            <div class="container mt-5 mb-5">
                <button class="btn btn-primary btn-sm" type="button" data-bs-target="#ModalTambah" data-bs-toggle="modal">Tambah Data</button>
                <asp:Repeater ID="RptTampilBarang" runat="server">
                    <HeaderTemplate>
                        <table id="myTable" class="table table-hover table-striped table-borderless ">
                        <thead>
                              <tr>
                                <th>Kode</th>
                                <th>Nama Barang</th>
                                <th>Satuan</th>
                                <th>Kategori</th>
                                <th>Detail</th>
                             </tr>
                        </thead>
                        <tbody>
                    </HeaderTemplate>

                        <ItemTemplate>
                            <tr>
                                    <td>
                                        <%# Eval("kodeBarang") %>

                                    </td>
                                    <td>
                                        <%# Eval("namaBarang") %>

                                    </td>
                                    <td>
                                        <%# Eval("satuan") %>

                                    </td>
                                    <td>
                                        <%# Eval("kategori") %>

                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-info btn-sm">Detail</a>
                                    </td>
                            </tr>
                        </ItemTemplate>

                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                

             

            </div>
            <!-- Modal -->
            <div class="modal fade" id="ModalTambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header alert-success">
                    <h5 class="modal-title" id="exampleModalLabel">Form Tambah Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                   <div class="form-group">
                       <asp:TextBox ID="TxtNamaBarang" CssClass="form-control mb-2" placeholder="Nama Barang" runat="server"></asp:TextBox>
                   </div>
                      <div class="form-group">
                          <asp:DropDownList ID="DlSatuan" CssClass="form-control mb-2" runat="server">
                          <asp:ListItem>-- Pilih Satuan --</asp:ListItem>
                          <asp:ListItem>Pcs</asp:ListItem>
                          <asp:ListItem>Box</asp:ListItem>
                          <asp:ListItem>Kaleng</asp:ListItem>
                        </asp:DropDownList>
                   </div>
                      <div class="form-group">
                        <asp:TextBox ID="TxtKategori" CssClass="form-control mb-2" placeholder="Kategori" runat="server"></asp:TextBox>
                   </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                      <asp:Button ID="BtnSave" OnClick="BtnSave_Click" CssClass="btn btn-primary" runat="server" Text="Simpan" />
                  </div>
                </div>
              </div>
            </div>  
        </main>
        <footer></footer>
        <div>
            
        </div>
    </form>

    <script src="bootstrap/js/jquery.dataTables.min.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="bootstrap/js/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    </script>
</body>
</html>
