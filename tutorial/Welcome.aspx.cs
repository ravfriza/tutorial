﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using tutorial.Classes;

namespace tutorial
{
    public partial class Welcome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TampilData();
            }
        }

        private void TampilData()
        {
            DataTable Dt = new DataTable();
            Dt = ClsWelcome.TampilData();
            RptTampilBarang.DataSource = Dt;
            RptTampilBarang.DataBind();
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            ClsWelcome.SimpanBarang(TxtNamaBarang.Text, DlSatuan.SelectedValue, TxtKategori.Text);
            TampilData();
            Bersih();

        }

        public void Bersih()
        {
            TxtNamaBarang.Text = string.Empty;
            TxtKategori.Text = string.Empty;
            DlSatuan.SelectedValue = "-- Pilih Satuan --";
        }
    }
}