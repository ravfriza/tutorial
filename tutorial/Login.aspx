﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="tutorial.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="additional-file/css/global.css" rel="stylesheet" />
</head>
<body>
    <div class="container-fluid bg-login">
        <div class="row">
            <div class="col-md-4 col-sm-12"></div>
            <div class="col-md-4 col-sm-12">

                <form class="container-form bg-white" runat="server">
                    <h3 class="text-center mb-5 fw-bold">Login!</h3>
              <div class="form-group mb-3">
                  <asp:TextBox ID="TxtUser" CssClass="form-control" placeholder="Username" runat="server"></asp:TextBox>
              </div>
              <div class="form-group mb-3">
                  <asp:TextBox ID="TxtPass" TextMode="Password" CssClass="form-control" placeholder="Password" runat="server"></asp:TextBox>                
              </div>
                <div class="d-grid gap-2">
                  <asp:Button ID="BtnLogin" OnClick="BtnLogin_Click" CssClass="btn btn-success mt-1" runat="server" Text="Login" />
                    <asp:Label ID="LblWarning" cssClass="text-danger text-center fw-light" runat="server" Text=""></asp:Label>
              </div>   
            </form>

            </div>
            <div class="col-md-4 col-sm-12"></div>
           

        </div>
    </div>

    
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
